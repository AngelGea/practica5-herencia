package clases;

public class MusicaEnDirecto extends Musica {

	//atributos
	
	int publico;
	String lugar;
	int precioEntradas;
	
	
	//constructores
	
	public MusicaEnDirecto() {
		super();
		this.publico = 0;
		this.lugar = "";
		this.precioEntradas = 0;
	}
	
	public MusicaEnDirecto(int intensidad, String tono, String timbre, double duracion, String ritmo, int publico, String lugar, int precioEntradas) {
			
			super(intensidad, tono,timbre,duracion,ritmo);
			
			this.publico = publico;
			this.lugar = lugar;
			this.precioEntradas = precioEntradas;
			
	}
	
	
	//setter y getter
	public int getPublico() {
		return publico;
	}

	public void setPublico(int publico) {
		this.publico = publico;
	}

	public String getLugar() {
		return lugar;
	}

	public void setLugar(String lugar) {
		this.lugar = lugar;
	}

	public int getPrecioEntradas() {
		return precioEntradas;
	}

	public void setPrecioEntradas(int precioEntradas) {
		this.precioEntradas = precioEntradas;
	}

	
	// to string
	@Override
	public String toString() {
		return "MusicaEnDirecto [publico=" + publico + ", lugar=" + lugar + ", precioEntradas=" + precioEntradas
				+ ", toString()=" + super.toString() + "]";
	}

	
		

	// metodos
	
	public int ganarPublico (String publicidad) {
		String publicidad1 = "Fanta";
		int publico1 = 120;
		String publicidad2 = "Cocacola";
		int publico2 = 1000;
		
		if(publicidad.equalsIgnoreCase(publicidad1)) {
			return publico1;
		} 
		
		if(publicidad.equalsIgnoreCase(publicidad2)) {
			return publico2;
		}
		
		return publico;
		
	}
	
	
}
