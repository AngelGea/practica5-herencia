package clases;

public class MusicaOnline extends Musica {
	
	// atributos
	
	int  oyentes;
	String plataforma;
	String publicidad;
	
	// constructor
	
	public MusicaOnline() {
		super();
		this.oyentes = 0;
		this.plataforma = "";
		this.publicidad = "";
	}

	public MusicaOnline(int intensidad, String tono, String timbre, double duracion, String ritmo, int oyentes, String plataforma, String publicidad) {
		
		super(intensidad, tono,timbre,duracion,ritmo);
		this.oyentes = oyentes;
		this.plataforma = plataforma;
		this.publicidad = publicidad;
	}
	
	
	// setter y getter
	public int getOyentes() {
		return oyentes;
	}

	public void setOyentes(int oyentes) {
		this.oyentes = oyentes;
	}

	public String getPlataforma() {
		return plataforma;
	}

	public void setPlataforma(String plataforma) {
		this.plataforma = plataforma;
	}

	public String getPublicidad() {
		return publicidad;
	}

	public void setPublicidad(String publicidad) {
		this.publicidad = publicidad;
	}


	// to string 


	@Override
	public String toString() {
		return "MusicaOnline [oyentes=" + oyentes + ", plataforma=" + plataforma + ", publicidad=" + publicidad
				+ ", toString()=" + super.toString() + "]";
	}
	
	
	
	


	// metodos
	
	public int ganarOyentes (String publicidad) {
		String publicidad1 = "Escuchenme";
		int oyentes1 = 80090;
		String publicidad2 = "Listen to me";
		int oyentes2 = 150000;
		
		if(publicidad.equalsIgnoreCase(publicidad1)) {
			return oyentes1;
		} 
		
		if(publicidad.equalsIgnoreCase(publicidad2)) {
			return oyentes2;
		}
		
		return oyentes;
		
	}
	
}

	

